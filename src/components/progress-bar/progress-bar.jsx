import React from 'react';

// Styles
import './progress-bar.scss';

export default function ProgressBar({percent}) {
  let border = parseInt(percent) > 99 ? {borderRadius: "6px 6px 0 0"} : {borderRadius: "6px 0 0 0"};
  border.width = `${percent}%`;

  return (
    <div className="ac-progress-bar">
      <div className="ac-progress-bar--status" data-testid="ac-progress-bar" style={border} role="progressbar" aria-valuenow={percent} aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  )
};