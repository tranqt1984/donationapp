import React from 'react';
import {cleanup, render} from '@testing-library/react';
import {afterEach, expect, it} from '@jest/globals';
import ProgressBar from './progress-bar';

afterEach(cleanup);

describe('<ProgressBar/>', () => {
  it('should renders without crashing', () => {
    render(<ProgressBar />);
  });

  it('should have the accessibility attribute of aria-valuenow', () => {
    const {getByTestId} = render(<ProgressBar />);
    expect(getByTestId('ac-progress-bar').hasAttribute('aria-valuenow'));
  });

  it('should have the percent value be 100', () => {
    const {getByTestId} = render(<ProgressBar percent={100} />);
    const progressBar = getByTestId('ac-progress-bar');
    expect(progressBar.getAttribute('aria-valuenow')).toBe('100');
  });
});
