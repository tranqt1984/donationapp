import React from 'react';
import {cleanup, render, fireEvent} from '@testing-library/react';
import {expect, it} from '@jest/globals';
import InputGroup from './input-group';

afterEach(cleanup);

describe('<InputGroup/>', () => {
  it('should renders without crashing', () => {
    render(<InputGroup />);
  });

  it('should have the accessibility attribute of aria-label set to "Amount (in dollar)"', () => {
    const {getByTestId} = render(<InputGroup />);
    const input = getByTestId('ac-input');
    expect(input.getAttribute('aria-label')).toBe('Amount (in dollar)');
  });

  it('should have the button label of "Click Me"', () => {
    const {getByTestId} = render(<InputGroup label="Click Me" />);
    const button = getByTestId('ac-input__button');
    expect(button.textContent).toBe('Click Me');
  });

  it('should have the currency set to a dollar sign', () => {
    const {getByTestId} = render(<InputGroup />);
    const currency = getByTestId('ac-input--currency');
    expect(currency.textContent).toBe('$');
  });

  it('should change the beenClicked variable to true after clicking on the button', () => {
    let beenClicked = false;
    const {getByTestId} = render(
      <InputGroup handleSubmit={() => (beenClicked = true)} />
    );
    const button = getByTestId('ac-input__button');
    fireEvent.click(button);
    expect(beenClicked).toBeTruthy();
  });

  it('should change the input value of 500', () => {
    const {getByTestId} = render(<InputGroup />);
    const input = getByTestId('ac-input');
    fireEvent.change(input, {target: {value: '500'}});
    expect(input.value).toBe('500');
  });
});
