import React from 'react';

// Styles
import './input-group.scss';

export default function InputGroup({label="Button label", handleSubmit, handleInputChange, handleEnterKey, amount}) {

  return (
    <div className="ac-input-group" data-testid="ac-input-group">
      <span className="ac-input-group--currency" data-testid="ac-input--currency">$</span>
      <input className="ac-input-group__input" data-testid="ac-input" type="text" placeholder="0" value={amount} onChange={handleInputChange} onKeyPress={handleEnterKey} aria-label="Amount (in dollar)"/>
      <button className="ac-input-group__button" data-testid="ac-input__button" onClick={handleSubmit} type="button">{label}</button>
    </div>
  )
};