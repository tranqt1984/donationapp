// Count down feature
export default function countDown(setDays, endDate) {
  const x = setInterval(function () {
    const now = new Date().getTime();
    const distance = endDate - now;
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));

    switch (days) {
      case 7:
        return setDays('seven');
      case 6:
        return setDays('six');
      case 5:
        return setDays('five');
      case 4:
        return setDays('four');
      case 3:
        return setDays('three');
      case 2:
        return setDays('two');
      case 1:
        return setDays('one');
      default:
        return setDays('EXPIRED');
    }
  }, 1000);
}
