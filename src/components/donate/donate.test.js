import React from 'react';
import {cleanup, render} from '@testing-library/react';
import {afterEach, expect, it} from '@jest/globals';
import Donate from './donate';

afterEach(cleanup);

describe('<Donate/>', () => {
  it('should renders without crashing', () => {
    render(<Donate />);
  });

  it('should have the default title of "loading..."', () => {
    const {getByTestId} = render(<Donate />);
    const title = getByTestId('ac-donate__title');
    expect(title.textContent).toBe('loading...');
  });

  it('should have the default description of "Join the 0 other donors who have already supported this project."', () => {
    const {getByTestId} = render(<Donate />);
    const title = getByTestId('ac-donate__description');
    expect(title.textContent).toBe(
      'Join the 0 other donors who have already supported this project.'
    );
  });

  it('should have the message be empty by default', () => {
    const {getByTestId} = render(<Donate />);
    const message = getByTestId('ac-donate__message');
    expect(message.textContent).toBe('');
  });
});
