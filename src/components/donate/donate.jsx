import React, {useState, useEffect} from 'react';

// Styles
import './donate.scss';

// Components
import Alert from '../alert/alert';
import InputGroup from '../input-group/input-group';
import ProgressBar from '../progress-bar/progress-bar';

// Helpers
import numberWithCommas from './helpers/number-with-commas';
import countDown from './helpers/count-down';

export default function Donate() {
  const donationGoal = 5000;
  const endDate = new Date("Feb 7, 2021 23:59:00").getTime();
  const strawberry = {color: "#e40e49"};
  const mint = {color: "#139879"};

  const [donors, setDonors] = useState(0);
  const [amount, setAmount] = useState("");
  const [total, setTotal] = useState(0);
  const [goal, setGoal] = useState(donationGoal);
  const [percent, setPercent] = useState(0);
  const [totalPercent, setTotalPercent] = useState(0);
  const [days, setDays] = useState("");
  const [messageColor, setMessageColor] = useState({})
  const [message, setMessage] = useState("");
  
  useEffect(() => {
    countDown(setDays, endDate);
    setPercent(amount / donationGoal);
  });

  const handleInputChange = (e) => {
    setAmount(e.target.value);
  };

  const handleEnterKey = (e) => {
    if (e.nativeEvent.keyCode === 13) {
      handleSubmit();
    }
  };

  const handleSubmit = () => {
    let preAmmount = parseInt(amount);

    function showMessage(msg, color){
      setMessage(msg);
      setMessageColor(color);
      setAmount("");
    }

    for (let i = 0; i < amount.length; i++) {
      let code = amount.charCodeAt(i);

      if(code > 57 || code < 48){
        setGoal(goal);
        setDonors(donors);
        return showMessage(`*Please enter a valid number between 0 and 9.`, strawberry);
      } else if ( parseInt(amount) >= 5 && parseInt(amount) <= (goal - 5) || parseInt(amount) === goal) {
        setDonors(donors + 1);
        setGoal(goal - amount);
        setTotalPercent(totalPercent + percent);
        setTotal(total + parseInt(amount));
        showMessage(`Success! Your donating of $${numberWithCommas(amount)} has been submitted.`, mint);
      };
    };

    if(parseInt(amount) < 5 ){
      showMessage(`*Your donation can't be below $5.`, strawberry);
    };

    if(parseInt(amount) >= 5 && parseInt(amount) > (goal)){
      showMessage(`*Donation can't exceed remaining funds needed for this project.`, strawberry);
    };

    if(parseInt(amount) > (goal - 5) && parseInt(amount) < (goal) && parseInt(amount) !== goal){
      showMessage(`*Your donation can't be between $${numberWithCommas(goal - 4)} and $${numberWithCommas(goal - 1)}.`, strawberry);
    };
  };

  let funds = numberWithCommas(goal);
  const title = days !== "" ? `Only ${days} days left to fund this project.` : "loading...";
  const description = <p>Join the <span className="ac-donate__description--donors">{donors}</span> other donors who have already supported this project.</p>;
  const alertLabel = <p><span className="ac-alert--dollar-sign">$</span><span className="ac-alert--funds">{funds}</span> still needed to fund this project</p>

  return (
    <div className="ac-donate" data-testid="ac-donate">
      <Alert alertStyles="ac-alert__donate ac-alert--primary ac-alert--carrot-br" pointer="ac-alert--primary ac-alert__pointer ac-alert__pointer--br" label={alertLabel}/>
      <ProgressBar percent={Math.floor(totalPercent * 100)}/>
      {title && <h1 className="ac-donate__title" data-testid="ac-donate__title">{title}</h1>}
      {description && <div className="ac-donate__description" data-testid="ac-donate__description">{description}</div>}
      <InputGroup label="Give Now" handleSubmit={handleSubmit} handleInputChange={handleInputChange} handleEnterKey={handleEnterKey} amount={amount}/>
      <p className="ac-donate__message" style={messageColor} role='alert' data-testid="ac-donate__message">{message}</p>
    </div>
  )
};

// Testing 123