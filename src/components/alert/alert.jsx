import React from 'react';

// Styles
import './alert.scss';

export default function Alert({label, alertStyles, pointer}) {
  let styles = `ac-alert ${alertStyles}`;
  return (
    <div className="ac-alert--container">
      <div className={styles} role="alert" data-testid="ac-alert">
        {label}
        <span className={pointer}></span>
      </div>
    </div>
  )
}
