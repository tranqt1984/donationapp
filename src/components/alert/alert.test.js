import React from 'react';
import {cleanup, getByText, render} from '@testing-library/react';
import {afterEach, expect, it} from '@jest/globals';
import Alert from './alert';

afterEach(cleanup);

describe('<Alert/>', () => {
  it('should renders without crashing', () => {
    render(<Alert />);
  });

  it('should have the label of "Earthquake! Duck, cover, and hold."', () => {
    const {getByTestId} = render(
      <Alert label="Earthquake! Duck, cover, and hold." />
    );

    const alert = getByTestId('ac-alert');
    expect(alert.textContent).toBe('Earthquake! Duck, cover, and hold.');
  });

  it('should have the accessibility role attribute set to alert', () => {
    const {getByTestId} = render(<Alert percent={100} />);
    const alert = getByTestId('ac-alert');
    expect(alert.getAttribute('role')).toBe('alert');
  });
});
