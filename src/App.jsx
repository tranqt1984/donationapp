import React from 'react';

// Styles
import './App.scss';

// Components
import Donate from './components/donate/donate';

function App() {
  return (
    <div className="ac-app">
      <Donate />
    </div>
  )
};

export default App;