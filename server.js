const express = require('express');
const app = express();
let port = process.env.PORT;
port === null || port === undefined ? (port = 4000) : null;

app.use(express.static('build'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + './build/index.html');
});

app.listen(port, () => console.log(`Server is now live on port ${port}`));
