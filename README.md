# Donation App

Donation app developed with React.js, bundled with Webpack, transcompiled by Babel, hosted on Heroku, version control by GitLab, accessibility scored 98% on Chrome Lighthouse, tested with Jest, styled with SCSS, and built with love.

## Rules

* Used React without Create React App :thumbsup:
* My work is live to view on Heroku :thumbsup:
* Source code is available on GitLab :thumbsup:

## Bonus Points:

* Used SCSS :thumbsup:
* Added accessibility :thumbsup:
* Enhanced the experience with success and error state :thumbsup:

## Links

* View on Heroku: https://hidden-fjord-11224.herokuapp.com/
* Code on GitLab: https://gitlab.com/tranqt1984/donationapp

## Lighthouse score screenshot
![Chrome lighthouse score](https://tinyurl.com/y5m4f47b "image to fork button")